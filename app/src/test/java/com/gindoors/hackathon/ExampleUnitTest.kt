package com.gindoors.hackathon

import org.junit.Test

import org.junit.Assert.*
import java.security.spec.PSSParameterSpec
import java.time.DayOfWeek
import java.time.LocalTime
import kotlin.collections.MutableList
import kotlin.collections.MutableMap

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */




class PSubject(var name: String, var max_absence: Int, var start: LocalTime, var end: LocalTime, var room: String) {
    var cur_absence = 0

    fun check_time(t: LocalTime):Boolean {
        return (t >= start && t <= end)
    }
    fun check_time():Boolean {
        return this.check_time(LocalTime.now())
    }
}

class PDay(var day: DayOfWeek) {
    var subjects: MutableList<PSubject> = arrayListOf()

    // Sprawdza czy czasy się nie nakładają
    fun add_subject(s: PSubject) {
        var start = s.start
        var end = s.end
        for (sub in subjects)
            if (sub.start <= end && sub.end >= start)
               throw DayOverlapException()
        subjects.add(s)
    }

    fun list_subjects():MutableList<PSubject> {
        return subjects
    }

    fun sort() {
        this.subjects = this.subjects.sortedWith(compareBy({it.start})).toMutableList()

    }
}

class PPlan() {
    var days: MutableMap<DayOfWeek, PDay> = mutableMapOf()

    fun add_day(d: PDay) {
        days.put(d.day, d)
    }
}

class ExampleUnitTest {
    @Test
    fun test() {
        var an = PSubject("Analiza Matematyczna", 2, LocalTime.of(8, 15,0), LocalTime.of(9,0,0), "314")
        var al = PSubject("Algebra", 3, LocalTime.of(7, 15,0), LocalTime.of(8,10,0), "313")

        if (an.check_time(LocalTime.of(8,20,0))) println("Trwa") else println("Nie trwa")

        if (an.check_time())
            println("Trwa")
        else
            println("Nie trwa")

        // Test Dnia
        var pon = PDay(DayOfWeek.MONDAY)
        pon.add_subject(an)
        pon.add_subject(al)

        pon.sort()

        for (sub in pon.list_subjects()){
            print (sub.name)
        }

        var plan = PPlan()
        plan.add_day(pon)

    }
}
