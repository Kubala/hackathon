package com.gindoors.hackathon
import java.time.DayOfWeek

/**
 * Created by Mateusz on 13.01.2018.
 * Class storing collection of subjects on given day of week
 */

class PDay(var day_of_week: Int) {
    var subjects: MutableList<PSubject> = arrayListOf()

    fun add_subject(s: PSubject) {
        var start_hh = s.start_hh
        var start_mm = s.start_mm
        var end_hh = s.end_hh
        var end_mm = s.end_mm
        var start_mins = start_hh*60 + start_mm
        var end_mins = end_hh*60 + end_mm
        // Check if times of different subjects on this day are overlapping
        for (sub in this.subjects) {
            var sub_start_mins = sub.start_hh * 60 + sub.start_mm
            var sub_end_mins = sub.end_hh * 60 + sub.end_mm
            if (sub_start_mins <= end_mins && sub_end_mins >= start_mins)
                throw DayOverlapException()
        }
        this.subjects.add(s)
    }

    fun list_subjects():MutableList<PSubject> {
        return this.subjects
    }

    fun sort() {
        print("SORT:"+this.subjects)
        this.subjects = this.subjects.sortedWith(compareBy({it.start_hh})).toMutableList()
    }
}