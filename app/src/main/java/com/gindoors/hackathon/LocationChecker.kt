package com.gindoors.hackathon

import android.icu.text.DateFormat
import android.icu.text.SimpleDateFormat
import com.indoorway.android.common.sdk.listeners.generic.Action1
import com.indoorway.android.common.sdk.model.IndoorwayPosition
import com.indoorway.android.location.sdk.IndoorwayLocationSdk
import java.util.*
import kotlin.system.measureTimeMillis

/**
 * Created by kamil on 13.01.18.
 * Class for checking if user is in classroom
 */
class LocationChecker(data: MapData, activity: MainActivity) {
    lateinit var listener: Action1<IndoorwayPosition>
    val roomLeavedCallback: RoomLeavedEvent = activity
    var mapData:MapData = data
    var actualRoom:String = ""
    var hoursOfEnter:Int = 0
    var minutesOfEnter:Int = 0
    var measurementStarted:Boolean = false
    var startCal: Calendar = Calendar.getInstance()
    fun startChecking(){
        listener = Action1<IndoorwayPosition> {
            if (mapData != null && mapData.mapData != null) {
                val result = mapData.mapData.objectsContainingCoordinates(it.coordinates)
                println("Number of elements: " + result.size)
                if (result.size > 0) {
                    var checkedRoom: String = ""
                    for (room: String in mapData.roomList) {
                        if (room == result[0].name) {
                            checkedRoom = room
                            break;
                        }
                    }

                    if(checkedRoom != actualRoom){
                        if(measurementStarted){
                            val endCal = Calendar.getInstance()
                            endCal.setTime(Date())
                            val hoursOfLeave = endCal.get(Calendar.HOUR_OF_DAY)
                            val minutesOfLeave = endCal.get(Calendar.MINUTE)
                            val dayOfWeek = endCal.get(Calendar.DAY_OF_WEEK)

                            roomLeavedCallback.OnLeave(RoomLeavedInfo(actualRoom,((endCal.timeInMillis - startCal.timeInMillis) / 60000).toLong(),hoursOfEnter,hoursOfLeave,minutesOfEnter,minutesOfLeave,dayOfWeek))

                        }
                        println("Room changed from " + actualRoom + " to " + checkedRoom)
                        startCal.setTime(Date())
                        actualRoom = checkedRoom
                        hoursOfEnter = startCal.get(Calendar.HOUR_OF_DAY)
                        minutesOfEnter = startCal.get(Calendar.MINUTE)

                        measurementStarted = true
                    }
                }
            }
        }
        onResume()
    }

    fun onPause(){
        IndoorwayLocationSdk.instance()
                .position()
                .onChange()
                .unregister(listener)
    }

    fun onResume(){
        IndoorwayLocationSdk.instance()
                .position()
                .onChange()
                .register(listener)
    }
}