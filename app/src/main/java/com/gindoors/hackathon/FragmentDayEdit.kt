package com.gindoors.hackathon


import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView


/**
 * A simple [Fragment] subclass.
 */
class FragmentDayEdit : Fragment() {

    public var day:String = ""
    public var dayNum:Int = 0

    lateinit var inflater: LayoutInflater
    lateinit var v: View
    var subList:MutableList<PSubject> = mutableListOf()
    val cardList:MutableList<View> = mutableListOf()
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v:View = inflater!!.inflate(R.layout.fragment_day_edit, container, false)

//        v.findViewById<TextView>(R.id.tempView).text = day

        this.v = v
        this.inflater = inflater

        val linearLayout = this.v.findViewById<LinearLayout>(R.id.cards_layout)

        for(elem:PSubject in subList){
            val child: View = this.inflater.inflate(R.layout.card,null)
            val lay:TextView = child.findViewById(R.id.subject)
            lay.text = elem.name
            println(elem.name)
            val startText:TextView = child.findViewById<TextView>(R.id.start)
            startText.text = elem.start_hh.toString()+":"+elem.start_mm.toString()
            val endText:TextView = child.findViewById<TextView>(R.id.end)
            endText.text = elem.end_hh.toString()+":"+elem.end_mm.toString()
            val linearLayout2 = child.findViewById<LinearLayout>(R.id.heart_layout)
            elem.cur_absence
            var i:Int = 0
            println(elem.max_absence)
            while(i<elem.max_absence-elem.cur_absence){
                val iv = ImageView(context)
                iv.setImageDrawable(resources.getDrawable(R.drawable.ic_heart_full,null))
                iv.imageTintList = ContextCompat.getColorStateList(activity,R.color.colorAccent)
                linearLayout2.addView(iv)
                i++
            }
            i = 0
            while(i<elem.cur_absence){
                val iv = ImageView(context)
                iv.setImageDrawable(resources.getDrawable(R.drawable.ic_heart,null))
                iv.imageTintList = ContextCompat.getColorStateList(activity,R.color.colorAccent)
                linearLayout2.addView(iv)
                i++
            }
            cardList.add(child)
            linearLayout.addView(child)
        }

//        val fab = this.v.findViewById<FloatingActionButton>(R.id.planEdit)
//        fab.setOnClickListener(this)


        return this.v


    }
    fun loadSubjectList(list: MutableList<PSubject>){
        subList = list
    }

}// Required empty public constructor
