package com.gindoors.hackathon

import com.indoorway.android.common.sdk.IndoorwaySdk
import com.indoorway.android.common.sdk.listeners.generic.Action1
import com.indoorway.android.common.sdk.model.IndoorwayMap

/**
 * Created by kamil on 13.01.18.
 * Class for managing data derived from map such as room numbers
 */
class MapData {
    var activity:MainActivity
    lateinit var mapData:IndoorwayMap
    val roomList:MutableList<String> = mutableListOf<String>()

    constructor(activity: MainActivity){
        IndoorwaySdk.initContext((activity))
        this.activity = activity
        IndoorwaySdk.configure((activity.getString(R.string.indoor_api_key)))
    }

    fun parse(){
        IndoorwaySdk.instance()
                .map()
                .details(activity.getString(R.string.building_key), activity.getString(R.string.second_floor_key))
                .setOnCompletedListener(Action1 {
                    mapData = it
                    val objects = mapData.objects
                    for (obj in objects){
                        if(obj.name!=null) {
                            val name: String = obj.name!!
                            if (name.contains("Room") || name.contains("ROOM"))
                                roomList.add(name)
                        }

                    }
                })
                .setOnFailedListener(Action1 {
                    // handle error, original exception is given on e.getCause()
                })
                .execute()

    }
}