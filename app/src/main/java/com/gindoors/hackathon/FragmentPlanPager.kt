package com.gindoors.hackathon


import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.support.v4.view.PagerTabStrip




/**
 * A simple [Fragment] subclass.
 */
class FragmentPlanPager : Fragment(), View.OnClickListener {

    lateinit var data:MapData
    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.addSubject->{
                val frag:FragmentSubjectEditor = FragmentSubjectEditor()
                frag.receiveRoomData(data)
                frag.changeDayText(adapterViewPager.getFragment(vp.currentItem).day,adapterViewPager.getFragment(vp.currentItem).dayNum)
                activity.supportFragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.push_up_in, R.anim.push_down_out,
                            R.anim.push_up_in, R.anim.push_down_out)
                    .replace(R.id.content_main, frag)
                    .addToBackStack(null)
                    .commit()
            }
        }
    }

    lateinit var vp:ViewPager
    lateinit var adapterViewPager:WeekDaysPagerAdapter
    var subList:MutableList<PSubject> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater!!.inflate(R.layout.fragment_plan_pager, container, false)

        vp = v.findViewById<ViewPager>(R.id.viewPager) as ViewPager

        adapterViewPager = WeekDaysPagerAdapter(childFragmentManager)
        val frag:FragmentDayEdit = FragmentDayEdit()
        frag.loadSubjectList(subList)
        adapterViewPager.add(frag,"Poniedziałek",1)
        adapterViewPager.add(FragmentDayEdit(), "Wtorek",2)
        adapterViewPager.add(FragmentDayEdit(),"Środa",3)
        adapterViewPager.add(FragmentDayEdit(), "Czwartek",4)
        adapterViewPager.add(FragmentDayEdit(),"Piątek",5)

        vp.adapter = adapterViewPager

        v.findViewById<FloatingActionButton>(R.id.addSubject).setOnClickListener(this)

        val pagerTabStrip = v.findViewById(R.id.pager_header) as PagerTabStrip
        pagerTabStrip.drawFullUnderline = true
        pagerTabStrip.tabIndicatorColor = resources.getColor(R.color.serce)

        return v
    }

    fun loadRoomData(data: MapData) {
        this.data = data
    }

    fun loadSubjectList(list: MutableList<PSubject>){
        subList = list
    }

}// Required empty public constructor
