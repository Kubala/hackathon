package com.gindoors.hackathon

/**
 * Created by kamil on 13.01.18.
 * Called after finish of db saving
 */
interface OnDataBaseSavingFinished {
    fun OnDBSaved(db:DataBaseHandler,day:PDay)
}