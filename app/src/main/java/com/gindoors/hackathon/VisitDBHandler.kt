package com.gindoors.hackathon

import android.content.Context
import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.util.*
import java.util.Calendar.getInstance

/**
 * Database table for visit records (ID, DATE, time of start, time of end)
 */
class VisitDBHandler(var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME,null,1){
    companion object {
        val DATABASE_NAME = "DBVisits"
        val TABLE_NAME = "Visits"
        val COL_ID = "ID"
        val COL_ROOM = "room"
        val COL_DAY_WEEK = "day_week"
        val COL_START_HH = "start_hh"
        val COL_START_MM = "start_mm"
        val COL_END_HH = "end_hh"
        val COL_END_MM = "end_mm"
        val COL_DURATION = "duration"
    }

    override fun onCreate(db: SQLiteDatabase?) {

        val createTable = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COL_ROOM + " VARCHAR(10)," +
            COL_DAY_WEEK + " INTEGER," +
            COL_START_HH + " INTEGER," +
            COL_START_MM + " INTEGER," +
            COL_END_HH + " INTEGER," +
            COL_END_MM + " INTEGER," +
            COL_DURATION + " INTEGER" + ");"
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?,oldVersion: Int,newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun insertData(record: VRecord) {
        val endCal = Calendar.getInstance()
        endCal.setTime(Date())
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(COL_START_HH, record.start_hh)
        cv.put(COL_START_MM, record.start_hh)
        cv.put(COL_END_HH, record.end_hh)
        cv.put(COL_END_MM, record.end_mm)
        cv.put(COL_DAY_WEEK, record.day_of_week)
        cv.put(COL_ROOM, record.room)
        cv.put(COL_DURATION, record.duration)
        var result = db.insert(TABLE_NAME, null, cv)

    }

    fun getSubjectDuration(room: String):Long {
        val db = this.readableDatabase
        val query = "SELECT SUM(" + COL_DURATION + ") FROM " + TABLE_NAME + " WHERE " + COL_ROOM + " = \"Room " + room +"\""
        val result = db.rawQuery(query, null)
        result.moveToFirst()
        return result.getLong(0)
    }

    fun getAllVisitsInRoom(room: String): MutableList<VRecord>{
        val db = this.readableDatabase
        val visits_in_room: MutableList<VRecord> = mutableListOf()
        val query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_ROOM + " = \"Room " + room +"\""
        val result = db.rawQuery(query, null)
        if(result.moveToFirst()) {
            do {
                val start_hh = result.getInt(result.getColumnIndex(COL_START_HH))
                val start_mm = result.getInt(result.getColumnIndex(COL_START_MM))
                val end_hh = result.getInt(result.getColumnIndex(COL_END_HH))
                val end_mm = result.getInt(result.getColumnIndex(COL_END_MM))
                val room = result.getString(result.getColumnIndex(COL_ROOM))
                val duration = result.getLong(result.getColumnIndex(COL_DURATION))
                val day_of_week = result.getInt(result.getColumnIndex(COL_DAY_WEEK))
                visits_in_room.add(VRecord(room, start_hh, start_mm, end_hh, end_mm, duration, day_of_week))
            }while (result.moveToNext())
        }
        return visits_in_room
    }

}