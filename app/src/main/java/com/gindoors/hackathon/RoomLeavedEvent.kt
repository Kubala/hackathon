package com.gindoors.hackathon

/**
 * Created by kamil on 13.01.18.
 * Listener called when user leaves room
 */
interface RoomLeavedEvent {
    fun OnLeave(info:RoomLeavedInfo)
}