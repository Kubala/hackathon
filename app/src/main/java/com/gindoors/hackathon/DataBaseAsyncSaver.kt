package com.gindoors.hackathon

import android.os.AsyncTask

/**
 * Created by kamil on 13.01.18.
 * Class for asynchronous adding data to DB
 */
class DataBaseAsyncSaver(private val activity: MainActivity,private val day: PDay) : AsyncTask<MutableList<PSubject>,Void,Boolean>(){


    var listener: OnDataBaseSavingFinished = activity as OnDataBaseSavingFinished

    override fun doInBackground(vararg p0: MutableList<PSubject>?): Boolean {
        val db = DataBaseHandler(activity)
        this.day.subjects = p0[0]!!
        db.insertData(this.day)
        listener.OnDBSaved(db,day)
        return true
    }
}