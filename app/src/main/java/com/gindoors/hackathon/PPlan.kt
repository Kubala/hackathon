package com.gindoors.hackathon

import java.time.DayOfWeek

/**
 * Created by Mateusz on 13.01.2018.
 */
class PPlan() {
    var days: MutableMap<Int, PDay> = mutableMapOf()

    fun add_day(d: PDay) {
        days.put(d.day_of_week, d)
    }
}