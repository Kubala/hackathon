package com.gindoors.hackathon

import android.os.AsyncTask

/**
 * Created by kamil on 13.01.18.
 * Class for asynchronous map data parsing
 */
class MapDataAsyncParser(): AsyncTask<MainActivity, Void, MapData>() {

    lateinit var mapData:MapData
    lateinit var listener: OnDataParsingFinishedInterface
    override fun doInBackground(vararg p0: MainActivity?): MapData {
        val mapData:MapData = MapData(p0[0]!!)
        listener = p0[0]!!
        mapData.parse()
        return mapData
    }

    override fun onPostExecute(result: MapData?) {
        super.onPostExecute(result)
        mapData = result!!
        listener.onFinish(mapData)
    }

}