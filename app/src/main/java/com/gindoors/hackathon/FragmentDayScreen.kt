package com.gindoors.hackathon


import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.support.v4.content.ContextCompat
import android.widget.*


/**
 * A simple [Fragment] subclass.
 */
class FragmentDayScreen : Fragment(), View.OnClickListener {
    lateinit var data:MapData
    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.planEdit -> {
                val frag = FragmentPlanPager()
                frag.loadRoomData(data)
                frag.loadSubjectList(subList)
                activity.supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.push_up_in, R.anim.push_down_out,
                                R.anim.push_up_in, R.anim.push_down_out)
                        .replace(R.id.content_main, frag)
                        .addToBackStack(null)
                        .commit()
            }
            R.id.statistics -> {
                val frag = FragmentStatisticsScreen()
                activity.supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.push_up_in, R.anim.push_down_out,
                                R.anim.push_up_in, R.anim.push_down_out)
                        .replace(R.id.content_main, frag)
                        .addToBackStack(null)
                        .commit()
            }
            R.id.showMap-> {
                val frag = FragmentMap()
                activity.supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.push_up_in, R.anim.push_down_out,
                                R.anim.push_up_in, R.anim.push_down_out)
                        .replace(R.id.content_main, frag)
                        .addToBackStack(null)
                        .commit()
            }
        }
        for (id:Int in cardIds) {
            if (id == p0?.id) {
                val frag = FragmentSubjectScreen()
                println(id)
                frag.loadData(subList[id-1])
                activity.supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.push_up_in, R.anim.push_down_out,
                                R.anim.push_up_in, R.anim.push_down_out)
                        .replace(R.id.content_main, frag)
                        .addToBackStack(null)
                        .commit()
            }
        }
    }
    lateinit var inflater: LayoutInflater
    lateinit var v: View
    var subList:MutableList<PSubject> = mutableListOf()
    val cardList:MutableList<View> = mutableListOf()
    var cardIds:MutableList<Int> = mutableListOf()
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater!!.inflate(R.layout.fragment_day_screen, container, false)
        this.v = v
        this.inflater = inflater

        val fab = v.findViewById<FloatingActionButton>(R.id.planEdit)
        fab.setOnClickListener(this)

        val linearLayout = this.v.findViewById<LinearLayout>(R.id.cards_layout)
        var count = 0
        if (subList.isNotEmpty()) {
            v.findViewById<ImageView>(R.id.tutorial).visibility = View.GONE
        }
        for(elem:PSubject in subList){
            val child: View = this.inflater.inflate(R.layout.card,null)
            val lay:TextView = child.findViewById(R.id.subject)
            lay.text = elem.name
            println(elem.name)
            val startText:TextView = child.findViewById<TextView>(R.id.start)
            startText.text = elem.start_hh.toString()+":"+elem.start_mm.toString()
            val endText:TextView = child.findViewById<TextView>(R.id.end)
            endText.text = elem.end_hh.toString()+":"+elem.end_mm.toString()
            val linearLayout2 = child.findViewById<LinearLayout>(R.id.heart_layout)
            elem.cur_absence
            var i:Int = 0
            println(elem.max_absence)
            while(i<elem.max_absence-elem.cur_absence){
                val iv = ImageView(context)
                iv.setImageDrawable(resources.getDrawable(R.drawable.ic_heart_full,null))
                iv.imageTintList = ContextCompat.getColorStateList(activity,R.color.colorAccent)
                linearLayout2.addView(iv)
                i++
            }
            i = 0
            while(i<elem.cur_absence){
                val iv = ImageView(context)
                iv.setImageDrawable(resources.getDrawable(R.drawable.ic_heart,null))
                iv.imageTintList = ContextCompat.getColorStateList(activity,R.color.colorAccent)
                linearLayout2.addView(iv)
                i++
            }

            val progressBar = child.findViewById<ProgressBar>(R.id.lifeBar)
            val anim = ProgressBarAnimation(progressBar, 0f, 50f)
            anim.duration = 1000
            progressBar.startAnimation(anim)

            cardIds.add(View.generateViewId())
            cardList.add(child)
            linearLayout.addView(child)
            child.findViewById<CardView>(R.id.card).id = cardIds.last()
            child.findViewById<CardView>(cardIds.last()).setOnClickListener(this)
        }
        val fab2 = this.v.findViewById<FloatingActionButton>(R.id.statistics)
        fab2.setOnClickListener(this)

        val fab3 = this.v.findViewById<FloatingActionButton>(R.id.showMap)
        fab3.setOnClickListener(this)

        val progressBar = v.findViewById<ProgressBar>(R.id.lifeBar)
        val anim = ProgressBarAnimation(progressBar, 0f, 200f)
        anim.duration = 2000
        progressBar.startAnimation(anim)

        return this.v
    }

    fun loadRoomData(data: MapData) {
        this.data = data
    }

    fun loadSubjectList(list: MutableList<PSubject>){
        subList = list
    }



}// Required empty public constructor
