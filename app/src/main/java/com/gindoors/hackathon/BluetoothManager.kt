package com.gindoors.hackathon

import android.bluetooth.BluetoothAdapter
import android.content.Intent

/**
 * Created by kamil on 13.01.18.
 * Class for turning on bluetooth module if needed
 */
class BluetoothManager {
    fun enable(activity: MainActivity){
        val REQUEST_ENABLE_BT = 1
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        if (!mBluetoothAdapter.isEnabled()) {

            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        }
    }
}