package com.gindoors.hackathon


/**
 * Created by Mateusz on 13.01.18.
 * Class PSubject containing information about a subject (i.e. max absence, time, teacher, room)
 */
class PSubject(var name: String, var max_absence: Int, var start_hh: Int, var start_mm: Int, var end_hh: Int, var end_mm: Int,  var room: String, var teacher: String, var sid: Int, var cur_absence: Int) {
    var start_mins = start_hh*60+start_mm
    var end_mins = end_hh*60+end_mm
    fun set_sid(idd: Int) {
        this.sid = idd
    }
//    fun check_time(t: LocalTime):Boolean {
//        return (t >= start && t <= end)
//    }
//    fun check_time():Boolean {
//        return this.check_time(LocalTime.now())
//    }
}
