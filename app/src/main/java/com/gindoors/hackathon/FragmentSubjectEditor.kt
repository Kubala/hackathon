package com.gindoors.hackathon


import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.day_screen_subject_template.*


/**
 * A simple [Fragment] subclass.
 */
class FragmentSubjectEditor : Fragment(), View.OnClickListener {
    override fun onClick(p0: View?) {
        println(startInput.text.toString().substring(3,5).toInt())
        println(endInput.text.toString().substring(0,2).toInt())
        println(endInput.text.toString().substring(3,5).toInt())
        var subject:PSubject = PSubject(subjectInput.text.toString(),
                absenceInput.text.toString().toInt(),startInput.text.toString().substring(0,2).toInt(),
                startInput.text.toString().substring(3,5).toInt(),
                endInput.text.toString().substring(0,2).toInt(),
                endInput.text.toString().substring(3,5).toInt(),
                roomInput.selectedItem.toString(),teacherInput.text.toString(),0,0)
        subject.cur_absence = 0
        var db = DataBaseAsyncSaver(activity as MainActivity, PDay(dayWeekNum))
        var list: MutableList<PSubject> = mutableListOf()
        list.add(subject)
        db.execute(list)
        activity.onBackPressed()
    }

    var dayWeek:String = ""
    var dayWeekNum:Int = 0
    lateinit var data: MapData
    lateinit var subjectInput: EditText
    lateinit var teacherInput: EditText
    lateinit var roomInput: Spinner
    lateinit var typeInput: Spinner
    lateinit var startInput: EditText
    lateinit var endInput: EditText
    lateinit var absenceInput:EditText

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v:View = inflater!!.inflate(R.layout.fragment_subject_editor, container, false)

        subjectInput = v.findViewById<EditText>(R.id.subject_input)
        var weekDay = v.findViewById<TextView>(R.id.weekDay)
        weekDay.text = dayWeek
        println(dayWeek)
        teacherInput = v.findViewById<EditText>(R.id.teacher_input)
        roomInput = v.findViewById<Spinner>(R.id.room_input)
        val adapter:ArrayAdapter<String> = ArrayAdapter(activity,R.layout.support_simple_spinner_dropdown_item,data.roomList)
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        roomInput.adapter = adapter
        typeInput = v.findViewById<Spinner>(R.id.type_input)
        startInput = v.findViewById<EditText>(R.id.start_input)
        endInput = v.findViewById<EditText>(R.id.end_input)
        absenceInput =  v.findViewById<EditText>(R.id.absence_input)
        val accept_button: FloatingActionButton = v.findViewById<FloatingActionButton>(R.id.save_subject_button)
        accept_button.setOnClickListener(this)
        return v
    }

    fun receiveRoomData(data: MapData){
        this.data = data

    }

    fun changeDayText(day:String,dayNum:Int){
        dayWeek = day
        dayWeekNum = dayNum

    }

}// Required empty public constructor
