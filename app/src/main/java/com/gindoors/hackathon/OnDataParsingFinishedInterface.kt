package com.gindoors.hackathon

/**
 * Created by kamil on 13.01.18.
 * Callback called after finish of map data parsing
 */
interface OnDataParsingFinishedInterface {
    fun onFinish(data:MapData)
}