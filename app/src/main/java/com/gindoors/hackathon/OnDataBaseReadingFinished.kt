package com.gindoors.hackathon

/**
 * Created by Mateusz on 14.01.2018.
 * Called after finish of db reading
 */
interface OnDataBaseReadingFinished {
    fun OnDBRead(db:DataBaseHandler,day:PDay)
}