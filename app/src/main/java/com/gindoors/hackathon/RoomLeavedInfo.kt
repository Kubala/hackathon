package com.gindoors.hackathon

/**
 * Created by kamil on 13.01.18.
 * Class with data from recently leaved room
 */
data class RoomLeavedInfo(val room: String, val duration: Long, val startHour:Int, val endHour:Int,val startMinute: Int, val endMinute:Int, val dayOfWeek:Int)