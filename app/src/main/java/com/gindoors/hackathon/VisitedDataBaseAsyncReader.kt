package com.gindoors.hackathon

import android.os.AsyncTask
import java.lang.Math.abs
import kotlin.math.abs
/**
 * Created by Mateusz on 14.01.18.
 * Class for asynchronous reading data
 * Checks for absence and duration of time spent on subjects
 */
class VisitedDataBaseAsyncReader(private val activity: MainActivity) : AsyncTask<String, Void, Boolean>(){
    var listener: OnVisitedDataBaseReadingFinished = activity as OnVisitedDataBaseReadingFinished

    override fun doInBackground(vararg p0: String?): Boolean {
        val room = p0[0]!!
        val dbr = DataBaseHandler(activity)
        val subject = dbr.getSubjectByRoom(room)
        val day_of_week = dbr.getDayOfWeek(subject)

        val db = VisitDBHandler(activity)
        //val duration = db.getSubjectDuration(room)
        val sub_start = subject.start_mins
        val sub_end = subject.end_mins

        for (visit in db.getAllVisitsInRoom(room)) {
            if (visit.day_of_week == day_of_week) {
                val vis_start = visit.start_hh * 60 + visit.start_mm
                val vis_end = visit.end_hh * 60 + visit.end_mm

                if (sub_start <= vis_end && sub_end >= vis_start) {
                    val diff = (vis_start - sub_start) + (vis_end - sub_end)
                    val duration_of_subject = visit.duration - diff
                    if (duration_of_subject > 0)
                        if (duration_of_subject <= 20 * 60) // 20 minutes -> absence
                            subject.cur_absence++
                }
            }
        }

        listener.OnVisitedDBRead(db, subject)
        return true
    }
}