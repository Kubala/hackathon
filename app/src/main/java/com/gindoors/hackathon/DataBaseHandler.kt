package com.gindoors.hackathon

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper



class DataBaseHandler(var context: Context) : SQLiteOpenHelper(context,DATABASE_NAME,null,1){

    companion object {
        val DATABASE_NAME ="DBSubjects"
        val TABLE_NAME="Subjects"
        val COL_NAME = "name"
        val COL_MAX_ABSENCE = "max_absence"
        val COL_CUR_ABSENCE = "cur_absence"
        val COL_START_HH = "start_hh"
        val COL_START_MM = "start_mm"
        val COL_END_HH = "end_hh"
        val COL_END_MM = "end_mm"
        val COL_ROOM = "room"
        val COL_TEACHER = "teacher"
        val COL_ID = "ID"
        val COL_DAY = "day"
    }


    override fun onCreate(db: SQLiteDatabase?) {

        val createTable = "CREATE TABLE " + TABLE_NAME +" (" +
                COL_ID +" INTEGER PRIMARY KEY AUTOINCREMENT," +
                COL_NAME + " VARCHAR(255)," +
                COL_MAX_ABSENCE + " INTEGER," +
                COL_CUR_ABSENCE + " INTEGER," +
                COL_START_HH + " INTEGER," +
                COL_START_MM + " INTEGER," +
                COL_END_HH + " INTEGER," +
                COL_END_MM + " INTEGER," +
                COL_DAY + " INTEGER," +
                COL_TEACHER + " VARCHAR(255)," +
                COL_ROOM +" VARCHAR(10)" + ");"
        println("DODAJE BAZE")
        db?.execSQL(createTable)

    }

    override fun onUpgrade(db: SQLiteDatabase?,oldVersion: Int,newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun insertData(day: PDay){
        val db = this.writableDatabase
        for (sub in day.subjects) {
            var cv = ContentValues()
            cv.put(COL_NAME, sub.name)
            cv.put(COL_MAX_ABSENCE, sub.max_absence)
            cv.put(COL_CUR_ABSENCE, sub.cur_absence)
            cv.put(COL_START_HH, sub.start_hh)
            cv.put(COL_START_MM, sub.start_hh)
            cv.put(COL_END_HH, sub.end_hh)
            cv.put(COL_END_MM, sub.end_mm)
            cv.put(COL_ROOM, sub.room)
            cv.put(COL_TEACHER, sub.teacher)
            cv.put(COL_DAY, day.day_of_week)
            var result = db.insert(TABLE_NAME, null, cv)
            var result_id = db.rawQuery("SELECT " + COL_ID + " FROM " + TABLE_NAME + " ORDER BY " + COL_ID + " DESC", null)
            result_id.moveToFirst()
            sub.sid = result_id.getInt(result_id.getColumnIndex(COL_ID))
        }
    }

    fun readData() : MutableList<PSubject>{
        val list : MutableList<PSubject> = ArrayList()

        val db = this.readableDatabase
        val query = "SELECT * FROM " + TABLE_NAME
        val result = db.rawQuery(query,null)
        if(result.moveToFirst()){
            do {
                val name = result.getString(result.getColumnIndex(COL_NAME))
                val max_absence = result.getInt(result.getColumnIndex(COL_MAX_ABSENCE))
                val cur_absence = result.getInt(result.getColumnIndex(COL_CUR_ABSENCE))
                val start_hh = result.getInt(result.getColumnIndex(COL_START_HH))
                val start_mm = result.getInt(result.getColumnIndex(COL_START_MM))
                val end_hh = result.getInt(result.getColumnIndex(COL_END_HH))
                val end_mm = result.getInt(result.getColumnIndex(COL_END_MM))
                val room = result.getString(result.getColumnIndex(COL_ROOM))
                val teacher = result.getString(result.getColumnIndex(COL_TEACHER))
                val sid = result.getInt(result.getColumnIndex(COL_ID))
                list.add(PSubject(name, max_absence, start_hh, start_mm, end_hh, end_mm, room, teacher, sid, cur_absence))
            }while (result.moveToNext())
        }

        result.close()
        db.close()
        return list
    }

    fun readDayOfWeek(day_of_week: Int) : PDay {
        val day = PDay(day_of_week)

        val db = this.readableDatabase
        val query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_DAY + " = " + day_of_week
        val result = db.rawQuery(query, null)
        if (result.moveToFirst()) {
            do {
                val name = result.getString(result.getColumnIndex(COL_NAME))
                val max_absence = result.getInt(result.getColumnIndex(COL_MAX_ABSENCE))
                val cur_absence = result.getInt(result.getColumnIndex(COL_CUR_ABSENCE))
                val start_hh = result.getInt(result.getColumnIndex(COL_START_HH))
                val start_mm = result.getInt(result.getColumnIndex(COL_START_MM))
                val end_hh = result.getInt(result.getColumnIndex(COL_END_HH))
                val end_mm = result.getInt(result.getColumnIndex(COL_END_MM))
                val room = result.getString(result.getColumnIndex(COL_ROOM))
                val teacher = result.getString(result.getColumnIndex(COL_TEACHER))
                val sid = result.getInt(result.getColumnIndex(COL_ID))
                day.add_subject(PSubject(name, max_absence, start_hh, start_mm, end_hh, end_mm, room, teacher, sid, cur_absence))
                println(name+" "+room+" "+teacher)
            } while (result.moveToNext())
        }
        day.sort()
        return day
    }

    fun getDayOfWeek(subject: PSubject):Int {
        val db = this.readableDatabase
        val query = "SELECT "+ COL_DAY+" FROM " + TABLE_NAME + " WHERE " + COL_ID + " = " + subject.sid
        val result = db.rawQuery(query, null)
        result.moveToFirst()
        val day_of_week = result.getInt(0)
        result.close()
        db.close()
        return day_of_week
    }

    fun getSubjectByRoom(room: String): PSubject {
        val db = this.readableDatabase
        val query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_ROOM + " = " + room
        println( "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_ROOM + " = " + room)
        val result = db.rawQuery(query, null)
        result.moveToFirst()
        val name = result.getString(result.getColumnIndex(COL_NAME))
        val max_absence = result.getInt(result.getColumnIndex(COL_MAX_ABSENCE))
        val cur_absence = result.getInt(result.getColumnIndex(COL_CUR_ABSENCE))
        val start_hh = result.getInt(result.getColumnIndex(COL_START_HH))
        val start_mm = result.getInt(result.getColumnIndex(COL_START_MM))
        val end_hh = result.getInt(result.getColumnIndex(COL_END_HH))
        val end_mm = result.getInt(result.getColumnIndex(COL_END_MM))
        val room = result.getString(result.getColumnIndex(COL_ROOM))
        val teacher = result.getString(result.getColumnIndex(COL_TEACHER))
        val sid = result.getInt(result.getColumnIndex(COL_ID))

        result.close()
        db.close()

        return PSubject(name, max_absence, start_hh, start_mm, end_hh, end_mm, room, teacher, sid, cur_absence)

    }

    fun deleteData(){
        val db = this.writableDatabase
        db.delete(TABLE_NAME,null,null)
        db.close()
    }
//
//
//    fun updateData() {
//        val db = this.writableDatabase
//        val query = "Select * from " + TABLE_NAME
//        val result = db.rawQuery(query,null)
//        if(result.moveToFirst()){
//            do {
//                var cv = ContentValues()
//                cv.put(COL_AGE,(result.getInt(result.getColumnIndex(COL_AGE))+1))
//                db.update(TABLE_NAME,cv,COL_ID + "=? AND " + COL_NAME + "=?",
//                        arrayOf(result.getString(result.getColumnIndex(COL_ID)),
//                                result.getString(result.getColumnIndex(COL_NAME))))
//            }while (result.moveToNext())
//        }
//
//        result.close()
//        db.close()
//    }


}