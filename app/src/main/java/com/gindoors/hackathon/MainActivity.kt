package com.gindoors.hackathon


import android.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.widget.LinearLayout
import android.widget.TextView
import com.indoorway.android.common.sdk.IndoorwaySdk


class MainActivity : AppCompatActivity(),OnDataParsingFinishedInterface, RoomLeavedEvent, OnDataBaseSavingFinished
,OnVisitedDataBaseSavingFinished, OnDataBaseReadingFinished{
    override fun OnVisitedDBSaved(db: VisitDBHandler) {
        println("saved visitation")
    }
    override fun OnDBRead(db: DataBaseHandler, day:PDay) {
        println("List of subjects: " + day.list_subjects().size)
        actualSubjects = day.list_subjects()
        dayScreenFrag.loadSubjectList(day.list_subjects())
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.content_main, dayScreenFrag, "Pager")
//        transaction.addToBackStack(PagerFrag.getTag());
        transaction.commit()

    }
    override fun OnDBSaved(db: DataBaseHandler, day: PDay) {
        println("List of subjects after save: " + day.list_subjects().size)
        val dbReader:DataBaseAsyncReader = DataBaseAsyncReader(this)
        var day = PDay(1)
        dbReader.execute(day)
    }


    override fun OnLeave(info: RoomLeavedInfo) {
            var rec = VRecord(info.room, info.startHour, info.startMinute, info.endHour, info.endMinute, info.duration, info.dayOfWeek)
            val dbSaver = VisitedDataBaseAsyncSaver(this)
            dbSaver.execute(rec)
            var i:Int = 0
            for(sub:PSubject in actualSubjects){
                println("Weszlo")
                println(info.room)
                println(sub.room)

                if(sub.room == info.room) {
                   dayScreenFrag.cardList[i].setBackgroundColor(ContextCompat.getColor(this,R.color.wyklad))
                }
                i++
            }
            info.room
    }


    lateinit var locationChecker:LocationChecker
    lateinit var dayScreenFrag:FragmentDayScreen
    lateinit var actualSubjects: MutableList<PSubject>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        IndoorwaySdk.initContext(this)
        // it's up to you when to initialize IndoorwaySdk, once initialized it will work forever!
        IndoorwaySdk.configure(getString(R.string.indoor_api_key))

        BluetoothManager().enable(this)
        PermissionsManager().check(this)
        setContentView(R.layout.activity_main)
        dayScreenFrag = FragmentDayScreen()
        val dbRead = DataBaseAsyncReader(this)
        dbRead.execute(PDay(1))
        val parser = MapDataAsyncParser()
        parser.execute(this)
    }

    override fun onFinish(data: MapData) {
        locationChecker = LocationChecker(data,this)
        locationChecker.startChecking()
        dayScreenFrag.loadRoomData(data)
    }

    override fun onPause() {
        super.onPause()

    }

    override fun onResume() {
        super.onResume()
    }

}



