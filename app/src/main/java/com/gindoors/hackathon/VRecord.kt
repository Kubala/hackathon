package com.gindoors.hackathon

import java.util.Calendar

/**
 * Visit record class
 */
class VRecord (var room: String, var start_hh: Int, var start_mm: Int, var end_hh: Int, var end_mm: Int, var duration: Long, var day_of_week: Int) {
    var start_mins = start_hh*60+start_mm
    var end_mins = end_hh*60+end_mm
}