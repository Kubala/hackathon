package com.gindoors.hackathon

/**
 * Created by Mateusz on 14.01.18.
 * Called after finish of visits DB reading
 */
interface OnVisitedDataBaseReadingFinished {
    fun OnVisitedDBRead(db: VisitDBHandler, sub: PSubject)
}