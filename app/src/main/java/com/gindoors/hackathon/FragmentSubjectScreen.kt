package com.gindoors.hackathon


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.PagerTabStrip
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.indoorway.android.common.sdk.listeners.generic.Action1
import com.indoorway.android.common.sdk.model.IndoorwayMap
import com.indoorway.android.map.sdk.view.IndoorwayMapView


/**
 * A simple [Fragment] subclass.
 */
class FragmentSubjectScreen : Fragment() {

    lateinit var data:PSubject
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v:View = inflater.inflate(R.layout.fragment_subject_screen, container, false)

        val teacherName = v.findViewById<TextView>(R.id.teacherName) as TextView
        teacherName.text = data.teacher
        val day = v.findViewById<TextView>(R.id.dayName2) as TextView
        day.text = "Poniedziałek"
        val include = v.findViewById<TextView>(R.id.subject) as TextView
        include.text = data.name


        val indoorwayMapView = v?.findViewById<IndoorwayMapView>(R.id.mapView)

        indoorwayMapView
                // optional: assign callback for map loaded event
                ?.onMapLoadCompletedListener = Action1<IndoorwayMap> { indoorwayMap ->
            // called on every new map load success
        }

        indoorwayMapView
                // perform map loading using building UUID and map UUID
                ?.load(getString(R.string.second_floor_key), getString(R.string.building_key))



        return v
    }

    fun loadData(data:PSubject) {
        this.data = data

    }

}// Required empty public constructor
