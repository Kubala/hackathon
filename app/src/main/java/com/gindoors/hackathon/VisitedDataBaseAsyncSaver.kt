package com.gindoors.hackathon

import android.os.AsyncTask

/**
 * Created by kamil on 13.01.18.
 * Class for asynchronous adding data to DB
 */
class VisitedDataBaseAsyncSaver(private val activity: MainActivity) : AsyncTask<VRecord, Void, Boolean>(){
    var listener: OnVisitedDataBaseSavingFinished = activity as OnVisitedDataBaseSavingFinished

    override fun doInBackground(vararg p0: VRecord?): Boolean {
        val db = VisitDBHandler(activity)
        db.insertData(p0[0]!!)
        listener.OnVisitedDBSaved(db)
        return true
    }
}