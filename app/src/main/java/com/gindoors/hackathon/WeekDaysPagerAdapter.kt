package com.gindoors.hackathon

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log
import kotlinx.android.synthetic.main.fragment_subject_screen.*

/**
 * Created by Kuba on 13.01.2018.
 * Pager for switching days of week in plan editor.
 */

class WeekDaysPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    companion object {
        private val NUM_ITEMS = 5

    }
    val fragmentList:MutableList<Fragment> = mutableListOf<Fragment>()
    fun add(fragment: FragmentDayEdit, day:String, dayNum:Int){
        fragment.day = day
        fragment.dayNum = dayNum
        fragmentList.add(fragment)
    }

    fun getFragment(id:Int):FragmentDayEdit{
        return fragmentList[id] as FragmentDayEdit
    }

    // Returns total number of pages
    override fun getCount(): Int {
        return NUM_ITEMS
    }

    // Returns the fragment to display for that page
    override fun getItem(position: Int): Fragment? {
        return this.fragmentList.get(position)
    }

    // Returns the page title for the top indicator
    override fun getPageTitle(position: Int): CharSequence {
        when (position) {
            0 -> return "Poniedziałek"
            1 -> return "Wtorek"
            2 -> return "Środa"
            3 -> return "Czwartek"
            4 -> return "Piątek"
        }
        return "Dzień"
    }
}
