package com.gindoors.hackathon

import android.os.AsyncTask

/**
 * Created by Mateusz on 14.01.18.
 * Class for asynchronous reading data from subject db
 */
class DataBaseAsyncReader(private val activity: MainActivity) : AsyncTask<PDay,Void,Boolean>(){
    var listener: OnDataBaseReadingFinished = activity as OnDataBaseReadingFinished

    override fun doInBackground(vararg p0: PDay?): Boolean {
        val db = DataBaseHandler(activity)
        var day = p0[0]!!
        day = db.readDayOfWeek(day.day_of_week)
        listener.OnDBRead(db,day)
        return true
    }
}